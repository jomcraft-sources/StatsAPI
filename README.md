[![Build Status](https://gitlab.com/jomcraft-sources/StatsAPI/badges/master/pipeline.svg)](https://gitlab.com/jomcraft-sources/StatsAPI)

### StatsAPI

---

StatsAPI is an opensource Java server backend (RestAPI) used by the Jomcraft Network development team to track certain stats concerning their projects. This software is not designed to run on infrastructure other than the JC-Network's own.

##### License

This product is licensed under the **Apache License v2.0** license. We do not grant any type of warranty.