package de.pt400c.statsapi;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;

@Path("/stats")
public class StatsHandler {
	
	public static HashMap<String, Long> perYearTimesDefaultSettings = new HashMap<String, Long>();
	public static HashMap<String, String> perYearDataDefaultSettings = new HashMap<String, String>();
	public static HashMap<String, Long> perYearTimesNoItemDespawn = new HashMap<String, Long>();
	public static HashMap<String, String> perYearDataNoItemDespawn = new HashMap<String, String>();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResponseJSON() {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("totalStartups", Integer.parseInt(getDefaultSettingsByID(true, "*")) + Integer.parseInt(getNoItemDespawnByID(true, "*")));

		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();

	}
	
	public static String formatNumbers(int number) {
		DecimalFormatSymbols customSymbols = DecimalFormatSymbols.getInstance(Locale.US); 
		customSymbols.setGroupingSeparator(' ');
		return new DecimalFormat("#,###;-#,###", customSymbols).format(number);
	}
	
	@GET
	@Path("noitemdespawn/endpoint")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResponseNID() {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("subject", "Startups");
		DecimalFormatSymbols customSymbols = DecimalFormatSymbols.getInstance(Locale.US); 
		customSymbols.setGroupingSeparator(' '); 
		jsonObject.put("status", formatNumbers(Integer.parseInt(getNoItemDespawnByID(true, "*"))));
		jsonObject.put("color", "8cba05");
		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();

	}
	
	@GET
	@Path("defaultsettings/endpoint")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResponseDS() {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("subject", "Startups");
		DecimalFormatSymbols customSymbols = DecimalFormatSymbols.getInstance(Locale.US); 
		customSymbols.setGroupingSeparator(' '); 
		jsonObject.put("status", formatNumbers(Integer.parseInt(getDefaultSettingsByID(true, "*"))));
		jsonObject.put("color", "8cba05");
		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();

	}
	
	public static String getNoItemDespawnByID(boolean total, String id) {
		if(perYearDataNoItemDespawn.containsKey(id)) {
			
			if((System.currentTimeMillis() - 3600000) > perYearTimesNoItemDespawn.get(id)) {
				String count = null;
				if(total)
					count = SQLStats.getNoItemDespawnTotalStartups();
				else
					count = SQLStats.getNoItemDespawnTotalStartups(id);
				perYearDataNoItemDespawn.put(id, count);
				perYearTimesNoItemDespawn.put(id, System.currentTimeMillis());
				return count;
			} else {
				return perYearDataNoItemDespawn.get(id);
			}
			
		} else {
			String count = null;
			if(total)
				count = SQLStats.getNoItemDespawnTotalStartups();
			else
				count = SQLStats.getNoItemDespawnTotalStartups(id);
			perYearDataNoItemDespawn.put(id, count);
			perYearTimesNoItemDespawn.put(id, System.currentTimeMillis());
			return count;
		}
	}
	
	public static String getDefaultSettingsByID(boolean total, String id) {
		if(perYearDataDefaultSettings.containsKey(id)) {
			
			if((System.currentTimeMillis() - 3600000) > perYearTimesDefaultSettings.get(id)) {
				String count = null;
				if(total)
					count = SQLStats.getDefaultSettingsTotalStartups();
				else
					count = SQLStats.getDefaultSettingsTotalStartups(id);
				perYearDataDefaultSettings.put(id, count);
				perYearTimesDefaultSettings.put(id, System.currentTimeMillis());
				return count;
			} else {
				return perYearDataDefaultSettings.get(id);
			}
			
		} else {
			String count = null;
			if(total)
				count = SQLStats.getDefaultSettingsTotalStartups();
			else
				count = SQLStats.getDefaultSettingsTotalStartups(id);
			perYearDataDefaultSettings.put(id, count);
			perYearTimesDefaultSettings.put(id, System.currentTimeMillis());
			return count;
		}
	}
	
	@GET
	@Path("noitemdespawn/endpoint/plain")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResponseNIDPlain() {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", Integer.parseInt(getNoItemDespawnByID(true, "*")));
		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();

	}
	
	@GET
	@Path("defaultsettings/endpoint/plain")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResponseDSPlain() {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", Integer.parseInt(getDefaultSettingsByID(true, "*")));
		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();

	}
	
	@GET
	@Path("defaultsettings/endpoint/{id}/plain")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResponseDS2Plain(@PathParam("id") String id) {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", Integer.parseInt(getDefaultSettingsByID(false, id)));
		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();

	}
	
	@GET
	@Path("noitemdespawn/endpoint/{id}/plain")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResponseNID2Plain(@PathParam("id") String id) {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", Integer.parseInt(getNoItemDespawnByID(false, id)));
		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();

	}
	
	@GET
	@Path("defaultsettings/endpoint/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResponseDS2(@PathParam("id") String id) {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("subject", "Startups");
		DecimalFormatSymbols customSymbols = DecimalFormatSymbols.getInstance(Locale.US); 
		customSymbols.setGroupingSeparator(' '); 
		jsonObject.put("status", formatNumbers(Integer.parseInt(getDefaultSettingsByID(false, id))));
		jsonObject.put("color", "8cba05");
		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();

	}
	
	@GET
	@Path("noitemdespawn/endpoint/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getResponseNID2(@PathParam("id") String id) {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("subject", "Startups");
		DecimalFormatSymbols customSymbols = DecimalFormatSymbols.getInstance(Locale.US); 
		customSymbols.setGroupingSeparator(' '); 
		jsonObject.put("status", formatNumbers(Integer.parseInt(getNoItemDespawnByID(false, id))));
		jsonObject.put("color", "8cba05");
		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();

	}

}