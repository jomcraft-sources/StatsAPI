package de.pt400c.statsapi;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.json.JSONException;
import org.json.JSONObject;

@Provider
public class ExceptionHandler implements ExceptionMapper<ClientErrorException> {

	public Response toResponse(ClientErrorException ex) throws JSONException {

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("error", "Request Error!");
		jsonObject.put("error-message", ex.getMessage());
		String result = jsonObject.toString();
		return Response.status(404).type("application/json").entity(result).build();
	}
}