package de.pt400c.statsapi;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
 
public class MySQL {
       
        private String HOST = "";
        private String DATABASE = "";
        private String USER = "";
        private String PASSWORD = "";
       
        public static Connection con;
       
        public MySQL(String host, String database, String user, String password) {
                this.HOST = host;
                this.DATABASE = database;
                this.USER = user;
                this.PASSWORD = password;
               
                connect();
        }
 
        public void connect() {
                try {
                        con = DriverManager.getConnection("jdbc:mariadb://" + this.HOST + ":3306/" + this.DATABASE + "?autoReconnect=true", this.USER, this.PASSWORD );
                     
                } catch (SQLException e) {
                	e.printStackTrace();
                	System.exit(1);
                }
        }
        
        public void close() {
                try {
                        if(con != null) {
                                con.close();
                             
                        }
                } catch (SQLException e) {
                	e.printStackTrace();
                	System.exit(1);
                }
        }
       
        public void update(String qry) {
                try {
                        Statement st = con.createStatement();
                        st.executeUpdate(qry);
                        st.close();
                } catch (SQLException e) {
                	
                	if(e.getMessage().startsWith("Could not create")) {
                        
                		e.printStackTrace();
                        	System.exit(1);
                        }else {
                        	connect();
                        	
                        }
                        
                }
        }
       
        public ResultSet query(String qry) {
                ResultSet rs = null;
               
                try {
                        Statement st = con.createStatement();
                        rs = st.executeQuery(qry);
                } catch (SQLException e) {
                        
                	if(e.getMessage().startsWith("Could not create")) {
                		e.printStackTrace();
                        	System.exit(1);
                        }else {
                        	connect();
                        
                        }
                       
                }
                return rs;
        }
}
