package de.pt400c.statsapi;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONObject;

@Path("/count")
public class CountHandler {
	
	public static HashMap<String, Integer> uploadQueryDS = new HashMap<String, Integer>();
	public static HashMap<String, Integer> uploadQueryNID = new HashMap<String, Integer>();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Response getIp(@Context HttpServletRequest request, String input) {

		if (input != null) {
			
			String id = null;
			String code = null;
			
			try {
				JSONObject jsonObject = new JSONObject(input);

				if (jsonObject.length() == 2) {
					id = jsonObject.getString("id");
					code = jsonObject.getString("code");
				} else {
					return Response.status(Response.Status.BAD_REQUEST).entity("Request malformed!").build();
				}
			} catch (Exception e) {
				return Response.status(Response.Status.BAD_REQUEST).entity("Request malformed!").build();
			}
			
			if(!(code.length() == 32))
				return Response.status(Response.Status.BAD_REQUEST).entity("Request malformed!").build();
			
			String remoteAddr = "ERROR";
	        
	        if (request != null) {
	            remoteAddr = request.getHeader("X-FORWARDED-FOR");
	            if (remoteAddr == null || "".equals(remoteAddr)) {
	                remoteAddr = request.getRemoteAddr();
	            }
	        }
	        
	        String address = remoteAddr.split(",")[0];
	        
	        userScored(id, address);

			return Response.status(Response.Status.ACCEPTED).entity("Accepted 202").build();

		} else {
			return Response.status(Response.Status.FORBIDDEN).entity("No data has been passed!").build();
		}

	}
	
	public static void userScored(String id, String address) {
		String usage_identifier = DigestUtils.sha256Hex(address);
		if (id.equals("Defaultsettings")) {
			synchronized (uploadQueryDS) {
				if (uploadQueryDS.containsKey(usage_identifier)) {
					uploadQueryDS.put(usage_identifier, uploadQueryDS.get(usage_identifier) + 1);
				} else {
					uploadQueryDS.put(usage_identifier, 1);
				}
			}
		} else if (id.equals("NoItemDespawn")) {
			synchronized (uploadQueryNID) {
				if (uploadQueryNID.containsKey(usage_identifier)) {
					uploadQueryNID.put(usage_identifier, uploadQueryNID.get(usage_identifier) + 1);
				} else {
					uploadQueryNID.put(usage_identifier, 1);
				}
			}
		}
	}

}