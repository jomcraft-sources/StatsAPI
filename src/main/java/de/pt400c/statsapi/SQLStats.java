package de.pt400c.statsapi;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLStats {
	
	public static String getGoToStatus() {
		try (ResultSet rs = StatsAPI.mysql.query("SELECT STATE FROM Goal_State WHERE NAME='server312.jomcraft.net';")) {
			if (rs.next()) {
				return rs.getString("STATE");
			}

			return "";
		} catch (NullPointerException e) {
			return getGoToStatus();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public static String getNoItemDespawnTotalStartups() {
		try (ResultSet rs = StatsAPI.mysql.query("SELECT TOTAL_STARTUPS FROM NoItemDespawn_Counts;")) {
			long startUps = 0;
			while (rs.next()) {
				startUps += Long.parseLong(rs.getString("TOTAL_STARTUPS"));
			}

			return "" + startUps;
		} catch (NullPointerException e) {
			return getNoItemDespawnTotalStartups();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "-1";
	}
	
	public static String getDefaultSettingsTotalStartups() {
		try (ResultSet rs = StatsAPI.mysql.query("SELECT TOTAL_STARTUPS FROM DefaultSettings_Counts;")) {
			long startUps = 0;
			while (rs.next()) {
				startUps += Long.parseLong(rs.getString("TOTAL_STARTUPS"));
			}

			return "" + startUps;
		} catch (NullPointerException e) {
			return getDefaultSettingsTotalStartups();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "-1";
	}

	public static String getDefaultSettingsTotalStartups(String year) {
		try (ResultSet rs = StatsAPI.mysql.query("SELECT TOTAL_STARTUPS FROM DefaultSettings_Counts WHERE YEAR='" + year + "';")) {
			while (rs.next()) {
				return rs.getString("TOTAL_STARTUPS");
			}

			return "0";
		} catch (NullPointerException e) {
			return getDefaultSettingsTotalStartups(year);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "-1";
	}
	
	public static String getNoItemDespawnTotalStartups(String year) {
		try (ResultSet rs = StatsAPI.mysql.query("SELECT TOTAL_STARTUPS FROM NoItemDespawn_Counts WHERE YEAR='" + year + "';")) {
			while (rs.next()) {
				return rs.getString("TOTAL_STARTUPS");
			}

			return "0";
		} catch (NullPointerException e) {
			return getNoItemDespawnTotalStartups(year);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "-1";
	}
	
	public static boolean userIdentifierExistsDS(String hash) {
		try {
			ResultSet rs = StatsAPI.mysql.query("SELECT USER_HASH FROM DefaultSettings_Users WHERE USER_HASH='" + hash + "';");
			if (rs.next()) {
				if (rs.getString("USER_HASH") != null)
					return true;
			}
			return false;
		} catch (NullPointerException e) {
			return userIdentifierExistsDS(hash);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean userIdentifierExistsNID(String hash) {
		try {
			ResultSet rs = StatsAPI.mysql.query("SELECT USER_HASH FROM NoItemDespawn_Users WHERE USER_HASH='" + hash + "';");
			if (rs.next()) {
				if (rs.getString("USER_HASH") != null)
					return true;
			}
			return false;
		} catch (NullPointerException e) {
			return userIdentifierExistsNID(hash);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static void finishQueryDS(String ip_hash, Integer integer) {
		if (userIdentifierExistsDS(ip_hash)) {
			StatsAPI.mysql.query("UPDATE DefaultSettings_Users SET COUNTS_SCORED = COUNTS_SCORED + " + integer + " WHERE USER_HASH='" + ip_hash + "';");
		} else {
			StatsAPI.mysql.update("INSERT INTO DefaultSettings_Users(USER_HASH, COUNTS_SCORED, COPIED_OVER) VALUES ('" + ip_hash + "', " + integer + ", 0);");
		}

	}
	
	public static void finishQueryNID(String ip_hash, Integer integer) {
		if (userIdentifierExistsNID(ip_hash)) {
			StatsAPI.mysql.query("UPDATE NoItemDespawn_Users SET COUNTS_SCORED = COUNTS_SCORED + " + integer + " WHERE USER_HASH='" + ip_hash + "';");
		} else {
			StatsAPI.mysql.update("INSERT INTO NoItemDespawn_Users(USER_HASH, COUNTS_SCORED, COPIED_OVER) VALUES ('" + ip_hash + "', " + integer + ", 0);");
		}

	}

	public static int processDefaultSettingsCounts(String year) {
		try {
			ResultSet rs = StatsAPI.mysql.query("SELECT * FROM DefaultSettings_Users;");
			int diff = 0;
			while (rs.next()) {
				String user = rs.getString("USER_HASH");
				int scored = rs.getInt("COUNTS_SCORED");
				int counted = rs.getInt("COPIED_OVER");
				diff += scored - counted;

				StatsAPI.mysql.query("UPDATE DefaultSettings_Users SET COPIED_OVER = COUNTS_SCORED WHERE USER_HASH='" + user + "';");
			}

			if (diff > 0) {
				StatsAPI.mysql.query("UPDATE DefaultSettings_Counts SET TOTAL_STARTUPS = TOTAL_STARTUPS + " + diff + " WHERE YEAR='" + year + "';");
			}
		} catch (NullPointerException e) {
			return processDefaultSettingsCounts(year);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public static int processNoItemDespawnCounts(String year) {
		try {
			ResultSet rs = StatsAPI.mysql.query("SELECT * FROM NoItemDespawn_Users;");
			int diff = 0;
			while (rs.next()) {
				String user = rs.getString("USER_HASH");
				int scored = rs.getInt("COUNTS_SCORED");
				int counted = rs.getInt("COPIED_OVER");
				diff += scored - counted;

				StatsAPI.mysql.query("UPDATE NoItemDespawn_Users SET COPIED_OVER = COUNTS_SCORED WHERE USER_HASH='" + user + "';");
			}

			if (diff > 0) {
				StatsAPI.mysql.query("UPDATE NoItemDespawn_Counts SET TOTAL_STARTUPS = TOTAL_STARTUPS + " + diff + " WHERE YEAR='" + year + "';");
			}
		} catch (NullPointerException e) {
			return processNoItemDespawnCounts(year);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
}