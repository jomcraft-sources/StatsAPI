package de.pt400c.statsapi;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

@Path("/")
public class StatusMessage {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response info() throws JSONException {
		
     
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("Status", "OK");
		jsonObject.put("Application-Author", "Jomcraft Network");
		jsonObject.put("Application-Description", "Jomcraft API v1");
		jsonObject.put("Specification-Version", "1.0.8");
		jsonObject.put("Application-Owner", "Jomcraft Network");

		String result = jsonObject.toString();
		return Response.status(200).entity(result).build();
	}

}
