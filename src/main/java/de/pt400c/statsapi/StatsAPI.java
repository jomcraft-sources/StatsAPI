package de.pt400c.statsapi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

public class StatsAPI {

	public static final Logger logger = LogManager.getLogger();
	public static MySQL mysql;
	public static Timer pushTimer = new Timer();
	public static Timer updateAPITimer = new Timer();	
	public static Timer aliveTimer = new Timer();	
	
	public static void startWarmup() {
		aliveTimer.scheduleAtFixedRate(new KeepAlive(), 5 * 60 * 1000, 5 * 60 * 1000);
		pushTimer.scheduleAtFixedRate(new PushTimer(), 13 * 60 * 100, 10 * 60 * 1000);
		updateAPITimer.scheduleAtFixedRate(new APITimer(), 59 * 60 * 1000, 60 * 60 * 1000);
	}

	public static void main(String[] args) {
		final Server server = new Server(Integer.valueOf(8281));
		final WebAppContext root = new WebAppContext();
		root.setContextPath("/");
		root.setParentLoaderPriority(true);

		final String webappDirLocation = "src/main/webapp/";
		root.setDescriptor(webappDirLocation + "/WEB-INF/web.xml");
		root.setResourceBase(webappDirLocation);
		server.setHandler(root);
		try {
			server.start();
			server.join();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
    
	public static class KeepAlive extends TimerTask {
		public void run() {
			String status = SQLStats.getGoToStatus();
			if(status.equals("SHUTDOWN") || status.equals("STOP")) {
				pushTimer.cancel();
				updateAPITimer.cancel();
				synchronized (CountHandler.uploadQueryDS) {
					for (String ip_hash : CountHandler.uploadQueryDS.keySet()) {
						SQLStats.finishQueryDS(ip_hash, CountHandler.uploadQueryDS.get(ip_hash));
					}
					CountHandler.uploadQueryDS.clear();
				}
				synchronized (CountHandler.uploadQueryNID) {
					for (String ip_hash : CountHandler.uploadQueryNID.keySet()) {
						SQLStats.finishQueryNID(ip_hash, CountHandler.uploadQueryNID.get(ip_hash));
					}
					CountHandler.uploadQueryNID.clear();
				}
				System.exit(0);
			}
		}
	}

	public static class APITimer extends TimerTask {
		public void run() {
			SQLStats.processDefaultSettingsCounts("" + Calendar.getInstance().get(Calendar.YEAR));
			SQLStats.processNoItemDespawnCounts("" + Calendar.getInstance().get(Calendar.YEAR));
		}
	}

	public static class PushTimer extends TimerTask {
		public void run() {
			synchronized (CountHandler.uploadQueryDS) {
				for (String ip_hash : CountHandler.uploadQueryDS.keySet()) {
					SQLStats.finishQueryDS(ip_hash, CountHandler.uploadQueryDS.get(ip_hash));
				}
				CountHandler.uploadQueryDS.clear();
			}
			synchronized (CountHandler.uploadQueryNID) {
				for (String ip_hash : CountHandler.uploadQueryNID.keySet()) {
					SQLStats.finishQueryNID(ip_hash, CountHandler.uploadQueryNID.get(ip_hash));
				}
				CountHandler.uploadQueryNID.clear();
			}
		}
	}

	static void connectMySQL() {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(new File("password.txt")));
			mysql = new MySQL("88.198.32.34", "StatsAPI", "StatsAPI", reader.readLine());
			startWarmup();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}