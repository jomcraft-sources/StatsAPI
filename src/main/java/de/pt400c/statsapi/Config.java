package de.pt400c.statsapi;

import org.glassfish.hk2.api.*;
import org.glassfish.jersey.server.ResourceConfig;
import javax.inject.Inject;
import javax.ws.rs.ApplicationPath;

@ApplicationPath("rest")
public class Config extends ResourceConfig {

    @Inject
    public Config(ServiceLocator locator) {
        DynamicConfigurationService dcs = locator.getService(DynamicConfigurationService.class);
        DynamicConfiguration config = dcs.createDynamicConfiguration();
        StatsAPI.connectMySQL();
        config.commit();

        packages(true, getClass().getPackage().getName());
    }
}